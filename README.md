## Домашнее задание `Паттерны декомпозиции микросервисов`

### Пользовательские сценарии
![Untitled Diagram.vpd-HTTP.png](./Сценарии%20соц%20сеть.jpg)

### Диаграмма взаидействий
![Untitled Diagram.vpd-HTTP.png](./диаграмма.png)
### Системные действия:
- **Пользователь** авторизуется
- **Пользователь** просматривает список **чатов** с другими **пользователями**
- **Пользователь** просматривает **чат** с **сообщениями** с другим **пользователем**
- **Пользователь** просматривает список **фотографий**
- **Пользователь** просматривает список **уведомлений**
- **Пользователь** просматривает **профиль**

---

### Итерация 0. 
#### Сервисы:
- Пользователи
- Профиль
- Сообщения
- Фотографии
- Уведомления
- Друзья
- Чаты

#### Сервис Пользователь

**Запросы:**

**Команды:**
- создание POST api/v1/user/create {login, email, password}
- блоникрование POST api/v1/user/block {id}
- авторизация  POST api/v1/user/login {user, password}

**События:**

**Зависимости:**

**Вопросы:**


#### Сервис Профиль
**Запросы:**
- получить профиль пользователя  POST api/v1/profile/get {userId}

**Команды:**
- редактировать POST api/v1/profile/edit {id, name, ...}

**События:**
- пользователь отредактировал профиль userChangedProfileInformation

**Зависимости:**

**Вопросы**

#### Сервис Сообщения
**Запросы:**
- получить последние сообщения в чате POST api/v1/message/getLast {chatId}
- получить все сообщения в чате POST api/v1/message/getAll {chatId}

**Команды:**
- написать сообщение в чате POST api/v1/message/write {text, chatId}
- удаить сообщеник DELETE api/v1/message {id}

**События:**
- польлзователь написал сообщение userSentMessage

**Зависимости:**

**Вопросы**

#### Сервис Чат
**Запросы:**
- получиь список сообщений POST api/v1/chat/getAll {userId}

**Команды:**
- создание POST api/v1/chat/ {userId, friendId}
- очистить чат POST api/v1/chat/clear {id}
- удалить чат DELETE api/v1/chat {id}

**События:**

**Зависимости:**

**Вопросы**

#### Сервис Фотографии
**Запросы:**
- получить список фотографий POST api/v1/photo/get {userId}

**Команды:**
- добавить фото POST api/v1/photo/add {content}
- удалить фото DELETE api/v1/photo {id}
- скрыть фото для остальных POST api/v1/hide {id}
- поделиться фото(отправить сообщением) POST api/v1/share {id, userId}

**События:**
- пользователь добавил фото userAddedPhoto

**Зависимости:**

**Вопросы**

#### Сервис Уведомления
**Запросы:**
- получить уведомления пользователя POST api/v1/notification/getAll {userId}

**Команды:**
- создание POST api/v1/notification/{userId, type, chatId}
- удалить(при просмотре) DELETE api/v1/notification {id}

**События:**

**Зависимости:**
- слушает событие userSentMessage
- слушает событие userAddedPhoto
- слушает событие userAddedFriend
- слушает событие userRemovedFriend
- слушает событие userChangedProfileInformation

#### Друзья
**Запросы:**
- получить список друзей POST api/v1/friend/getAll {userId}

**Команды:**
- добавить пользователя в друзья POST api/v1/friend/add {userId}
- удалить из друзей POST api/v1/friend/add {userId}

**События:**
- добавление нового друга userAddedFriend
- удаление из друзей 	userRemovedFriend

**Зависимости:**

 ---

### Итерация 1. Сервисы:
- Пользователи
- Профиль
- Сообщения
- Фотографии
- Уведомления

#### Сервис Пользователь
**Запросы:**
- получить список друзей POST api/v1/friend/getAll {userId}

**Команды:**
- создание POST api/v1/user/create {login, email, password}
- блоникрование POST api/v1/user/block {id}
- авторизация  POST api/v1/user/login {user, password}
- добавить пользователя в друзья POST api/v1/user/addFriend {userId}
- удалить из друзей POST api/v1/friend/add {userId}

**События:**
- добавление нового друга userAddedFriend
- удаление из друзей 	userRemovedFriend

**Зависимости:**

#### Сервис Профиль
**Запросы:**
- получить профиль пользователя  POST api/v1/profile/get {userId}

**Команды:**
- редактировать POST api/v1/profile/edit {id, name, ...}

**События:**
- пользователь отредактировал профиль userChangedProfileInformation

**Зависимости:**

#### Сервис Сообщения
**Запросы:**
- получить последние сообщения в чате POST api/v1/message/getLast {chatId}
- получить все сообщения в чате POST api/v1/message/getAll {chatId}
- получиь список чатов POST api/v1/message/chats {userId}

**Команды:**
- написать сообщение в чате POST api/v1/message/write {text, chatId}
- удалить сообщеник DELETE api/v1/message {id}
- очистить чат POST api/v1/message/clearChat {id}
- удалить чат DELETE api/v1/message /chat {id}

**События:**
- польлзователь написал сообщение userSentMessage

**Зависимости:**

#### Сервис Фотографии
**Запросы:**
- получить список фотографий POST api/v1/photo/get {userId}

**Команды:**
- добавить фото POST api/v1/photo/add {content}
- удалить фото DELETE api/v1/photo {id}
- скрыть фото для остальных POST api/v1/hide {id}
- поделиться фото(отправить сообщением) POST api/v1/share {id, userId}

**События:**
- пользователь добавил фото userAddedPhoto

**Зависимости:**

#### Сервис Уведомления
**Запросы:**
- получить уведомления пользователя POST api/v1/notification/getAll {userId}

**Команды:**
- создание POST api/v1/notification/{userId, type, chatId}
- удалить(при просмотре) DELETE api/v1/notification {id}

**События:**

**Зависимости:**
- слушает событие userSentMessage
- слушает событие userAddedPhoto
- слушает событие userAddedFriend
- слушает событие userRemovedFriend
- слушает событие userChangedProfileInformation